
def extract_from_this_table():

    with open("downloaded_data/aminoacids.raw") as f:
        amino_acid_dict = {}
        for row in f:
            split_row = row.split("\t")
            amino_acid_dict[split_row[0]] = split_row[4]
        print amino_acid_dict

if __name__ == "__main__":
    extract_from_this_table()