list1 = ['L-Lysine', 'L-Alanine', 'L-Aspartic acid', 'Glycine', 'L-Glutamic acid', 'L-Valine', 
         'L-Arginine', 'L-Asparagine', 'L-Tryptophan', 'L-Leucine', 'L-Threonine', 'L-Cysteine',  
         'L-Serine', 'L-Glutamine', 'L-Methionine', 'L-Isoleucine', 'L-Phenylalanine', 'L-Proline', 
         'L-Histidine', 'L-Tyrosine', '4-Hydroxyproline']

list2 = []

for element in list1:
    if element == "Glycine":
        list2.append(element.lower())
    else:      
        list2.append(element.capitalize())
    
for x, y in zip(list1, list2):
    print "'"+x+"':['"+y+"'],",