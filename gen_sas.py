#!/usr/bin/python
###################################################################
# Script to generate Secondary Alkyl Sulfonates (SAS).
# https://books.google.ca/books?id=SCWLcVDjxxsC&pg=PA13&lpg=PA13&dq=Secondary+Alkyl+Sulfonates+(SAS)&source=bl&ots=lUxwxdAm7B&sig=dZXac2rFE72fF5_xvTJdEMQsIbg&hl=en&sa=X&ved=0ahUKEwjr26bXkcfPAhVMxWMKHUuOD5UQ6AEIKTAB#v=onepage&q=Secondary%20Alkyl%20Sulfonates%20(SAS)&f=false
#
# David Arndt, Oct 2016
###################################################################

import os, sys, re, string
import time
from optparse import OptionParser
from rdkit import Chem
from rdkit.Chem import AllChem

extend_alkyl = "[#6;A;X4;H3:1]>>[#6;A;X4:1]C" # add carbon to end of alkyl chain
#sulfonate_transformation = "[#6;A;X4;H2:1]>>[#6;A;X4:1]S(=O)(=O)O" # add sulfonyl group to middle of alkyl chain
sulfonate_transformation_cw = "[#6;A;X4;H2:1]>>[#6@@;A;X4:1]S(=O)(=O)O" # add sulfonyl group to middle of alkyl chain, CW
sulfonate_transformation_ccw = "[#6;A;X4;H2:1]>>[#6@;A;X4:1]S(=O)(=O)O" # add sulfonyl group to middle of alkyl chain, CCW

# length of alkyl chain:
min_C = 10
max_C = 18

def transform(smarts_transformation, base_molecule):
    rxn = AllChem.ReactionFromSmarts(smarts_transformation)
    return rxn.RunReactants((base_molecule,))

def remove_duplicates(products):
    products_hash = {}
    
    for i in range(0, len(products)):
        smiles = Chem.MolToSmiles(products[i][0], isomericSmiles=True, kekuleSmiles=True)
        if smiles not in products_hash:
            products_hash[smiles] = products[i][0]
    return products_hash

# Main program
if __name__ == '__main__':

    usage = "usage: gen_sas.py -o outfile\n"
    
    if len(sys.argv)<3:
        print usage
        sys.exit(0)

    parser = OptionParser(usage)
    parser.add_option("-o", "--outfile", dest="outfile", help="Output file")
    (options, args) = parser.parse_args()
    outfile = options.outfile
    
    # Create array of dicts, one for each level of the reaction.
    levels = max_C-min_C+1
    hashlist = [dict() for x in range(levels)]
    
    # Base structure (C10):
    smiles = 'CCCCCCCCCC'
    
    hashlist[0][smiles] = Chem.Kekulize(Chem.MolFromSmiles(smiles)) # set consisting of initial structure
    
    # Extend alkyl chains
    start_time = time.time()
    for i in range(1,levels):
        print "extend alkyl chain " + str(i)
        for key in hashlist[i-1]:
            ps = transform(extend_alkyl, Chem.MolFromSmiles(key))
            ps_hash_tmp = remove_duplicates(ps)
            hashlist[i].update(ps_hash_tmp)
    end_time = time.time()
    diff_time = end_time - start_time
    print "  done in %f seconds" % diff_time
    
    hashlist2 = [dict() for x in range(levels)]
    
    # Attach sulfonyl group
    print "adding sulfonyl group"
    start_time = time.time()
    for i in range(levels):
        for key in hashlist[i]:
            ps = transform(sulfonate_transformation_cw, Chem.MolFromSmiles(key))
            ps_hash_tmp = remove_duplicates(ps)
            hashlist2[i].update(ps_hash_tmp)
            ps = transform(sulfonate_transformation_ccw, Chem.MolFromSmiles(key))
            ps_hash_tmp = remove_duplicates(ps)
            hashlist2[i].update(ps_hash_tmp)
    end_time = time.time()
    diff_time = end_time - start_time
    print "  done in %f seconds" % diff_time
    
    # Output structures
    output = open(outfile, 'w')
    count = 0
    for i in range(levels):
        for key in hashlist2[i]:
            
            print >>output, key
            count += 1
    
    print "generated " + str(count) + " products"
    
    
