# Script to generate every combination of cyclic dipeptides from the SMILES listed in amino_acid_dict.
#
# Output is in the "/outputs2" folder.
#
# Currently generation using Selenocysteine and beta-aspartate are disabled via commented out code.
# Selenocysteine is very rare, so finding any cyclic dipeptides containing selenocysteine is unlikely.
#
# Written by Elvis Lo January 2018.

import os, re, csv
from rdkit import Chem
from rdkit.Chem import AllChem
from datetime import datetime
start_time = datetime.now()

#amino_acid_dict contains SMILES of amino acids, taken from HMDB. These are all L-isomers.
amino_acid_dict = {'L-Lysine': 'NCCCC[C@H](N)C(O)=O', 'L-Alanine': 'C[C@H](N)C(O)=O', 'L-Aspartic acid': 'N[C@@H](CC(O)=O)C(O)=O', 
                   'Glycine': 'NCC(O)=O', 'L-Glutamic acid': 'N[C@@H](CCC(O)=O)C(O)=O', 'L-Valine': 'CC(C)[C@H](N)C(O)=O', 
                   'L-Arginine': 'N[C@@H](CCCNC(N)=N)C(O)=O', 'L-Asparagine': 'N[C@@H](CC(N)=O)C(O)=O', 
                   'L-Tryptophan': 'N[C@@H](CC1=CNC2=CC=CC=C12)C(O)=O', 'L-Leucine': 'CC(C)C[C@H](N)C(O)=O', 
                   'L-Threonine': 'C[C@@H](O)[C@H](N)C(O)=O', 'L-Cysteine': 'N[C@@H](CS)C(O)=O', 
                   'L-Serine': 'N[C@@H](CO)C(O)=O', 'L-Glutamine': 'N[C@@H](CCC(N)=O)C(O)=O', 'L-Methionine': 'CSCC[C@H](N)C(O)=O', 
                   'L-Isoleucine': 'CC[C@H](C)[C@H](N)C(O)=O', 'L-Phenylalanine': 'N[C@@H](CC1=CC=CC=C1)C(O)=O', 'L-Proline': 'OC(=O)[C@@H]1CCCN1', 
                   'L-Histidine': 'N[C@@H](CC1=CN=CN1)C(O)=O', 'L-Tyrosine': 'N[C@@H](CC1=CC=C(O)C=C1)C(O)=O', 
                   'L-4-Hydroxyproline' : 'O[C@H]1CN[C@@H](C1)C(O)=O', 'L-3-Hydroxyproline' : 'O[C@H]1CCN[C@H]1C(O)=O'} #, 'Selenocysteine': 'N[C@@H](C[SeH])C(O)=O'}

#As of February 2018, this dict is the same as in "generate_tripeptides.py"
amino_acid_synonym_dict = {'L-Lysine':['L-lysine', 'lysine', 'L-lysyl', 'lysyl', 'L-lys', 'lys', 'K'], 
                           'L-Alanine':['L-alanine', 'alanine', 'L-alanyl', 'alanyl', 'L-ala', 'ala', 'A'], 
                           'L-Aspartic acid':['L-aspartic acid', 'aspartic acid', 'L-aspartyl', 'aspartyl', 'L-asp', 'asp', 'N'], 
                           'Glycine':['glycine', 'glycine', 'glycyl', 'glycyl', 'gly', 'gly', 'G'], 
                           'L-Glutamic acid':['L-glutamic acid', 'glutamic acid', 'L-glutamyl', 'glutamyl', 'L-glu', 'glu', 'E'], 
                           'L-Valine':['L-valine', 'valine', 'L-valyl', 'valyl', 'L-val', 'val', 'V'], 
                           'L-Arginine':['L-arginine', 'arginine', 'L-arginyl', 'arginyl', 'L-arg', 'arg', 'R'], 
                           'L-Asparagine':['L-asparagine', 'asparagine', 'L-asparaginyl', 'asparaginyl', 'L-asn', 'asn', 'N'], 
                           'L-Tryptophan':['L-tryptophan', 'tryptophan', 'L-tryptophyl', 'tryptophyl', 'L-trp', 'trp', 'W'], 
                           'L-Leucine':['L-leucine', 'leucine', 'L-leucyl', 'leucyl', 'L-leu', 'leu', 'L'], 
                           'L-Threonine':['L-threonine', 'threonine', 'L-threoninyl', 'threoninyl', 'L-thr', 'thr', 'T'], 
                           'L-Cysteine':['L-cysteine', 'cysteine', 'L-cysteinyl', 'cysteinyl', 'L-cys', 'cys', 'C'], 
                           'L-Serine':['L-serine', 'serine', 'L-serinyl', 'serinyl', 'L-ser', 'ser', 'S'], 
                           'L-Glutamine':['L-glutamine', 'glutamine', 'L-glutaminyl', 'glutaminyl', 'L-gln', 'gln', 'Q'], 
                           'L-Methionine':['L-methionine', 'methionine', 'L-methionyl', 'methionyl', 'L-met', 'met', 'M'], 
                           'L-Isoleucine':['L-isoleucine', 'isoleucine', 'L-isoleucyl', 'isoleucyl', 'L-ile', 'ile', 'I'], 
                           'L-Phenylalanine':['L-phenylalanine', 'phenylalanine', 'L-phenylalanyl', 'phenylalanyl', 'L-phe', 'phe', 'F'], 
                           'L-Proline':['L-proline', 'proline', 'L-prolyl', 'prolyl', 'L-pro', 'pro', 'P'], 
                           'L-Histidine':['L-histidine', 'histidine', 'L-histidinyl', 'histidinyl', 'L-his', 'his', 'H'], 
                           'L-Tyrosine':['L-tyrosine', 'tyrosine', 'L-tyrosyl', 'tyrosyl', 'L-tyr', 'tyr', 'Y'], 
                           'L-4-Hydroxyproline':['L-4-hydroxyproline', 'hydroxyproline', 'L-4-hydroxyprolinyl', 'hydroxyprolinyl', 'L-4-hyp', 'hyp', ''],
                           'L-3-Hydroxyproline':['L-3-hydroxyproline', '-3-hydroxyproline', 'L-3-hydroxyprolinyl', '-3-hydroxyprolinyl', 'L-3-hyp', '3hyp', ''],
                           'Gamma-glutamate':['L-gamma-glutamate', 'gamma-glutamate', 'L-gamma-glutamyl', 'gamma-glutamyl', 'L-gamma-glu', 'gamma-glu', ''],
                           'Beta-aspartate':['L-beta-aspartate', 'beta-aspartate', 'L-beta-aspartyl', 'beta-aspartyl', 'L-beta-asp', 'beta-asp', '']}

def make_cyclic_dipeptide(amino_acid_1, amino_acid_2):
    
    cyclic_2x_condensation_reaction = AllChem.ReactionFromSmarts("[#7:1]-[#6:2]-[#6:3](-[#8:4])=[#8:9].[#7:5]-[#6:6]-[#6:7](-[#8:8])=[#8:10] >> "\
                                                                 "[CR1:7](=[O:10])1-[NR1:1]-[CR1:2]-[CR1:3](=[O:9])-[NR1:5]-[CR1:6]-1."\
                                                                 "[H][#8:4][H].[H][#8:8][H]")

    print "Condensing %s and %s." % (amino_acid_1, amino_acid_2)

    aa1_mol = Chem.MolFromSmiles(amino_acid_1)
    aa2_mol = Chem.MolFromSmiles(amino_acid_2) 
    
    products = cyclic_2x_condensation_reaction.RunReactants((aa1_mol, aa2_mol))
    
    if len(products) != 1:
        print "Possible products:", [Chem.MolToSmiles(element[0]) for element in products]
        raise Exception("Testing Error: Number of possible product sets not one. len(products):", len(products))    
    
    main_product = Chem.MolToSmiles(products[0][0])
    print main_product
    print Chem.MolToSmiles(products[0][1]) #This should be water    
    print Chem.MolToSmiles(products[0][2]) #This should also be water.    
    
    return main_product

def make_cyclic_dipeptide_with_gamma_glutamate(amino_acid_1):
    '''Gamma glutamate is glutamate where a peptide bond is connected to the gamma carbon.'''
    
    L_Glutamic_acid = amino_acid_dict["L-Glutamic acid"]
    
    cyclic_reaction_with_gamma_glutamate = AllChem.ReactionFromSmarts("[#7:1]-[#6:2]-[#6:3](-[#8:4])=[#8:9]."\
                                                                      "[#7:5]-[#6:6]-[#6:11]-[#6:12]-[#6:7](-[#8:8])=[#8:10] >> "\
                                                                      "[CR1:7](=[O:10])1-[NR1:1]-[CR1:2]-[CR1:3](=[O:9])-[NR1:5]-[CR1:6]"\
                                                                      "[CR1:11]-[CR1:12]-1.[H][#8:4][H].[H][#8:8][H]")
    
    print "Condensing %s and %s." % (amino_acid_1, L_Glutamic_acid)
    
    aa1_mol = Chem.MolFromSmiles(amino_acid_1)
    aa2_mol = Chem.MolFromSmiles(L_Glutamic_acid)
    
    products = cyclic_reaction_with_gamma_glutamate.RunReactants((aa1_mol, aa2_mol))
    
    if len(products) != 1:
        print "Possible products:", [Chem.MolToSmiles(element[0]) for element in products]
        raise Exception("Testing Error: Number of possible product sets not one. len(products):", len(products))        

    main_product = Chem.MolToSmiles(products[0][0])
    print main_product
    print Chem.MolToSmiles(products[0][1]) #This should be water    
    print Chem.MolToSmiles(products[0][2]) #This should also be water.    
    
    return main_product    

def make_cyclic_dipeptide_with_beta_aspartate(amino_acid_1):
    '''Beta aspartate is aspartate where a peptide bond is connected to the beta carbon.'''
    
    L_aspartic_acid = amino_acid_dict["L-Aspartic acid"]

    cyclic_reaction_with_beta_aspartate = AllChem.ReactionFromSmarts("[#7:1]-[#6:2]-[#6:3](-[#8:4])=[#8:9]."\
                                                                      "[#7:5]-[#6:6]-[#6:11]-[#6:7](-[#8:8])=[#8:10] >> "\
                                                                      "[CR1:7](=[O:10])1-[NR1:1]-[CR1:2]-[CR1:3](=[O:9])-[NR1:5]-[CR1:6]"\
                                                                      "[CR1:11]-1.[H][#8:4][H].[H][#8:8][H]")

    print "Condensing %s and %s." % (amino_acid_1, L_aspartic_acid)

    aa1_mol = Chem.MolFromSmiles(amino_acid_1)
    aa2_mol = Chem.MolFromSmiles(L_aspartic_acid)

    products = cyclic_reaction_with_beta_aspartate.RunReactants((aa1_mol, aa2_mol))

    if len(products) != 1:
        print "Possible products:", [Chem.MolToSmiles(element[0]) for element in products]
        raise Exception("Testing Error: Number of possible product sets not one. len(products):", len(products))        

    main_product = Chem.MolToSmiles(products[0][0])
    print main_product
    print Chem.MolToSmiles(products[0][1]) #This should be water    
    print Chem.MolToSmiles(products[0][2]) #This should also be water.    

    return main_product        

def generate_cyclic_dipeptide_synonyms(dipeptide_combination_string):
    
    global amino_acid_synonym_dict
    
    aa_list = re.split(" (?!acid)", dipeptide_combination_string)
    
    if len(aa_list) != 2:
        raise ValueError("Cyclic dipeptide with more or less than 2 amino acids:", aa_list)    
    
    name_list = [] 
    
    name_list.append("cyclo("+"-".join([amino_acid_synonym_dict[aa_list[0]][4], amino_acid_synonym_dict[aa_list[1]][4]]).title() + ")")  
    
    name_list.append("cyclo("+"-".join([amino_acid_synonym_dict[aa_list[0]][5], amino_acid_synonym_dict[aa_list[1]][5]]).title() + ")")  
    
    name_list.append("cyclo("+"-".join([amino_acid_synonym_dict[aa_list[0]][2], amino_acid_synonym_dict[aa_list[1]][2]]).title() + ")") 
    
    name_list.append("cyclo("+"".join([amino_acid_synonym_dict[aa_list[0]][3], amino_acid_synonym_dict[aa_list[1]][3]]) + ")") 
    
    name_list.append("cyclo("+"-".join([amino_acid_synonym_dict[aa_list[0]][3], amino_acid_synonym_dict[aa_list[1]][3]]).title() + ")")
    
    #Reverse
    
    name_list.append("cyclo("+"-".join([amino_acid_synonym_dict[aa_list[1]][4], amino_acid_synonym_dict[aa_list[0]][4]]).title() + ")")  
    
    name_list.append("cyclo("+"-".join([amino_acid_synonym_dict[aa_list[1]][5], amino_acid_synonym_dict[aa_list[0]][5]]).title() + ")")  
    
    name_list.append("cyclo("+"-".join([amino_acid_synonym_dict[aa_list[1]][2], amino_acid_synonym_dict[aa_list[0]][2]]).title() + ")") 
    
    name_list.append("cyclo("+"".join([amino_acid_synonym_dict[aa_list[1]][3], amino_acid_synonym_dict[aa_list[0]][3]]) + ")") 
    
    name_list.append("cyclo("+"-".join([amino_acid_synonym_dict[aa_list[1]][3], amino_acid_synonym_dict[aa_list[0]][3]]).title() + ")")    
    
    if "Gamma-glutamate" not in dipeptide_combination_string and "Beta-aspartate" not in dipeptide_combination_string:
        name_list.append("".join([amino_acid_synonym_dict[aa_list[0]][3], amino_acid_synonym_dict[aa_list[1]][3]]).title() + " diketopiperazine")
        
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][5], amino_acid_synonym_dict[aa_list[1]][5]]).title() + "-DKP") 
        
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][3], amino_acid_synonym_dict[aa_list[1]][3]]).title() + " diketopiperazine")
    
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][4], amino_acid_synonym_dict[aa_list[1]][4]]).title() + "-DKP") 
    
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][2], amino_acid_synonym_dict[aa_list[1]][2]]).title() + " diketopiperazine")
        
        #Reverse
        
        name_list.append("".join([amino_acid_synonym_dict[aa_list[1]][3], amino_acid_synonym_dict[aa_list[0]][3]]).title() + " diketopiperazine")
    
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[1]][5], amino_acid_synonym_dict[aa_list[0]][5]]).title() + "-DKP") 
    
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[1]][3], amino_acid_synonym_dict[aa_list[0]][3]]).title() + " diketopiperazine")
    
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[1]][4], amino_acid_synonym_dict[aa_list[0]][4]]).title() + "-DKP") 
    
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[1]][2], amino_acid_synonym_dict[aa_list[0]][2]]).title() + " diketopiperazine")        
    
    #Remove any duplicates. eg. for "Glycine Glycine"
    new_name_list = []
    for element in name_list:
        if element not in new_name_list:
            new_name_list.append(element.strip("-").replace("--", "-"))
    name_list = new_name_list   

    return name_list

def make_cyclic_dipeptides():
    
    global amino_acid_dict
    
    cyclic_dipeptide_list = []
    combinations_list = []
    
    for amino_acid_1 in amino_acid_dict:
        for amino_acid_2 in amino_acid_dict:
            print "Making cyclic dipeptide out of %s and %s." % (amino_acid_1, amino_acid_2)
            cyclic_dipeptide = make_cyclic_dipeptide(amino_acid_dict[amino_acid_1], amino_acid_dict[amino_acid_2])
            cyclic_dipeptide_list.append(cyclic_dipeptide)
            combinations_list.append(amino_acid_1 + " " + amino_acid_2)
        
        #Handle gamma-glutamate as a special case.
        print "Making cyclic dipeptide out of %s and %s." % (amino_acid_1, "Gamma-glutamate")
        cyclic_dipeptide = make_cyclic_dipeptide_with_gamma_glutamate(amino_acid_dict[amino_acid_1])
        cyclic_dipeptide_list.append(cyclic_dipeptide)
        combinations_list.append(amino_acid_1 + " " + "Gamma-glutamate")       
        
        #Handle beta-aspartate as a special case.
        #print "Making cyclic dipeptide out of %s and %s." % (amino_acid_1, "Beta-aspartate")
        #cyclic_dipeptide = make_cyclic_dipeptide_with_beta_aspartate(amino_acid_dict[amino_acid_1])
        #cyclic_dipeptide_list.append(cyclic_dipeptide)
        #combinations_list.append(amino_acid_1 + " " + "Beta-aspartate")
    
    if not os.path.exists("outputs2"):
        os.makedirs("outputs2")
            
    #Make two versions of output file, one with more details to make it easier to check for correctness.
    with open("outputs2/generated_cyclic_dipeptides.txt", "wb") as output_f, \
         open("outputs2/generated_cyclic_dipeptides_detailed.tsv", "wb") as output_f2, \
         open("outputs2/generated_cyclic_dipeptides_more_detailed.csv", "wb") as output_f3:
        
        csv_w = csv.DictWriter(output_f3, fieldnames=["SMILES", "Amino Acids used", "Names"])
        csv_w.writeheader()        
        
        for cyclic_dipeptide, combination in zip(cyclic_dipeptide_list, combinations_list):
            output_f.write(cyclic_dipeptide + "\r\n")
            output_f2.write(combination + "\t" + cyclic_dipeptide + "\r\n") 
            csv_w.writerow({"SMILES":cyclic_dipeptide, "Amino Acids used":combination, 
                            "Names":"; ".join(generate_cyclic_dipeptide_synonyms(combination))})

if __name__ == "__main__":
    make_cyclic_dipeptides()
    print "Finished in", datetime.now() - start_time