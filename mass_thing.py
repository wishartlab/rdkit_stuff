from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Descriptors

a = "C[Si](C)(C)OC[C@H]1O[C@H](OC[C@H]2OC(O[Si](C)(C)C)[C@H](O[Si](C)(C)C)[C@@H](O[Si](C)(C)C)[C@@H]2O)[C@H](O)[C@@H](O)[C@H]1O"
mol1 = Chem.MolFromSmiles(a)
print Descriptors.ExactMolWt(mol1)


b = "[#6][Si]([#6])([#6])[#8]-[#6]-[#6@H]-1-[#8]-[#6@H](-[#8]-[#6]-[#6@H]-2-[#8]-[#6](-[#8][Si]([#6])([#6])[#6])-[#6@H](-[#8][Si]([#6])([#6])[#6])-[#6@@H](-[#8][Si]([#6])([#6])[#6])-[#6@@H]-2-[#8])-[#6@H](-[#8])-[#6@@H](-[#8])-[#6@H]-1-[#8]"
mol2 = Chem.MolFromSmarts(b)

print mol2

c = "[#6][Si]([#6])([#6])[#8]-[#6]-[#6@H]-1-[#8]-[#6@H](-[#8]-[#6]-[#6@H]-2-[#8]-[#6](-[#8][Si]([#6])([#6])[#6])-[#6@H](-[#8])-[#6@@H](-[#8][Si]([#6])([#6])[#6])-[#6@@H]-2-[#8])-[#6@H](-[#8][Si]([#6])([#6])[#6])-[#6@@H](-[#8])-[#6@H]-1-[#8]"
mol3 = Chem.MolFromSmarts(c)

mol2.UpdatePropertyCache(strict=False)
mol3.UpdatePropertyCache(strict=False)

print Descriptors.ExactMolWt(mol2)
print Descriptors.ExactMolWt(mol3)


d = "[CH4]"

mol4 = Chem.MolFromSmarts(d)
mol4.UpdatePropertyCache(strict=False)

print Descriptors.ExactMolWt(mol4) * 2

e = "[CH0]-[CH0]"

mol5 = Chem.MolFromSmarts(e)
mol5.UpdatePropertyCache(strict=False)
print Descriptors.ExactMolWt(mol5)