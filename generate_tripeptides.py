# Script to generate every combination of tripeptides from the SMILES listed in amino_acid_dict.
#
# Output is in the "/outputs" folder.
#
# Not the most efficient algorithm, but it's fast enough to not be an issue. (Many dipeptides are created multiple times)
#
# Note that gamma-glutamate and beta-aspartate are about where they're peptide bonds are rather than separate compounds.
# At the time of writing, a good description of gamma-glutamate can be found here: https://en.wikipedia.org/wiki/Polyglutamic_acid 
# beta-aspartate is similar.
#
# HMDB11737 is an example of a dipeptide containing gamma-glutamate.
#
# Beta-aspartate seems to exist based on: 
# http://onlinelibrary.wiley.com/doi/10.1002/bip.1981.360200804/pdf
# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2782781/
# In vitro experiment: https://www.ncbi.nlm.nih.gov/pubmed/20737625
# Mentions it has been detected in human eyes:
# https://www.ncbi.nlm.nih.gov/pubmed/10706893
# A paper that has a good summary on the topic of beta-asparatate:
# https://www.ncbi.nlm.nih.gov/pubmed/7813483
#
# Regarding the code, the sections not about gamma-glutamate and beta-aspartate are the main functionality, commenting out some small
# amounts of code can be done to disable the extraneous functionality, since everything is run from generate_tripeptides()
#
# Currently Selenocysteine is disabled, as it is very rare and probably wouldn't be found in tripeptides.
# 
# I decided to keep beta-aspartate enabled, as I found literature which indicates it exists in proteins and peptides. The danger is
# that since beta-aspartate is not that well studied, we wouldn't know if there are any specific tripeptides in which it wouldn't exist.
#
# First written by Elvis Lo in December 2017.

import os, csv, re
from rdkit import Chem
from rdkit.Chem import AllChem
from datetime import datetime
start_time = datetime.now()

#amino_acid_dict contains SMILES of amino acids, taken from HMDB. These are all L-isomers.
amino_acid_dict = {'L-Lysine': 'NCCCC[C@H](N)C(O)=O', 'L-Alanine': 'C[C@H](N)C(O)=O', 'L-Aspartic acid': 'N[C@@H](CC(O)=O)C(O)=O', 
                   'Glycine': 'NCC(O)=O', 'L-Glutamic acid': 'N[C@@H](CCC(O)=O)C(O)=O', 'L-Valine': 'CC(C)[C@H](N)C(O)=O', 
                   'L-Arginine': 'N[C@@H](CCCNC(N)=N)C(O)=O', 'L-Asparagine': 'N[C@@H](CC(N)=O)C(O)=O', 
                   'L-Tryptophan': 'N[C@@H](CC1=CNC2=CC=CC=C12)C(O)=O', 'L-Leucine': 'CC(C)C[C@H](N)C(O)=O', 
                   'L-Threonine': 'C[C@@H](O)[C@H](N)C(O)=O', 'L-Cysteine': 'N[C@@H](CS)C(O)=O', 
                   'L-Serine': 'N[C@@H](CO)C(O)=O', 'L-Glutamine': 'N[C@@H](CCC(N)=O)C(O)=O', 'L-Methionine': 'CSCC[C@H](N)C(O)=O', 
                   'L-Isoleucine': 'CC[C@H](C)[C@H](N)C(O)=O', 'L-Phenylalanine': 'N[C@@H](CC1=CC=CC=C1)C(O)=O', 'L-Proline': 'OC(=O)[C@@H]1CCCN1', 
                   'L-Histidine': 'N[C@@H](CC1=CN=CN1)C(O)=O', 'L-Tyrosine': 'N[C@@H](CC1=CC=C(O)C=C1)C(O)=O', 
                   'L-4-Hydroxyproline' : 'O[C@H]1CN[C@@H](C1)C(O)=O', 'L-3-Hydroxyproline' : 'O[C@H]1CCN[C@H]1C(O)=O'} #, 'Selenocysteine': 'N[C@@H](C[SeH])C(O)=O'}

#For naming tripeptides and their synonyms. glycyl-glycyl-glycine will have to be dealt with as a special case.
#Keeping in partial lowercase is so that title() can be applied optionally depending on how we want it to look like.
amino_acid_synonym_dict = {'L-Lysine':['L-lysine', 'lysine', 'L-lysyl', 'lysyl', 'L-lys', 'lys', 'K'], 
                           'L-Alanine':['L-alanine', 'alanine', 'L-alanyl', 'alanyl', 'L-ala', 'ala', 'A'], 
                           'L-Aspartic acid':['L-aspartic acid', 'aspartic acid', 'L-aspartyl', 'aspartyl', 'L-asp', 'asp', 'N'], 
                           'Glycine':['glycine', 'glycine', 'glycyl', 'glycyl', 'gly', 'gly', 'G'], 
                           'L-Glutamic acid':['L-glutamic acid', 'glutamic acid', 'L-glutamyl', 'glutamyl', 'L-glu', 'glu', 'E'], 
                           'L-Valine':['L-valine', 'valine', 'L-valyl', 'valyl', 'L-val', 'val', 'V'], 
                           'L-Arginine':['L-arginine', 'arginine', 'L-arginyl', 'arginyl', 'L-arg', 'arg', 'R'], 
                           'L-Asparagine':['L-asparagine', 'asparagine', 'L-asparaginyl', 'asparaginyl', 'L-asn', 'asn', 'N'], 
                           'L-Tryptophan':['L-tryptophan', 'tryptophan', 'L-tryptophyl', 'tryptophyl', 'L-trp', 'trp', 'W'], 
                           'L-Leucine':['L-leucine', 'leucine', 'L-leucyl', 'leucyl', 'L-leu', 'leu', 'L'], 
                           'L-Threonine':['L-threonine', 'threonine', 'L-threoninyl', 'threoninyl', 'L-thr', 'thr', 'T'], 
                           'L-Cysteine':['L-cysteine', 'cysteine', 'L-cysteinyl', 'cysteinyl', 'L-cys', 'cys', 'C'], 
                           'L-Serine':['L-serine', 'serine', 'L-serinyl', 'serinyl', 'L-ser', 'ser', 'S'], 
                           'L-Glutamine':['L-glutamine', 'glutamine', 'L-glutaminyl', 'glutaminyl', 'L-gln', 'gln', 'Q'], 
                           'L-Methionine':['L-methionine', 'methionine', 'L-methionyl', 'methionyl', 'L-met', 'met', 'M'], 
                           'L-Isoleucine':['L-isoleucine', 'isoleucine', 'L-isoleucyl', 'isoleucyl', 'L-ile', 'ile', 'I'], 
                           'L-Phenylalanine':['L-phenylalanine', 'phenylalanine', 'L-phenylalanyl', 'phenylalanyl', 'L-phe', 'phe', 'F'], 
                           'L-Proline':['L-proline', 'proline', 'L-prolyl', 'prolyl', 'L-pro', 'pro', 'P'], 
                           'L-Histidine':['L-histidine', 'histidine', 'L-histidinyl', 'histidinyl', 'L-his', 'his', 'H'], 
                           'L-Tyrosine':['L-tyrosine', 'tyrosine', 'L-tyrosyl', 'tyrosyl', 'L-tyr', 'tyr', 'Y'], 
                           'L-4-Hydroxyproline':['L-4-hydroxyproline', 'hydroxyproline', 'L-4-hydroxyprolinyl', 'hydroxyprolinyl', 'L-4-hyp', 'hyp', ''],
                           'L-3-Hydroxyproline':['L-3-hydroxyproline', '-3-hydroxyproline', 'L-3-hydroxyprolinyl', '-3-hydroxyprolinyl', 'L-3-hyp', '3hyp', ''],
                           'Gamma-glutamate':['L-gamma-glutamate', 'gamma-glutamate', 'L-gamma-glutamyl', 'gamma-glutamyl', 'L-gamma-glu', 'gamma-glu', ''],
                           'Beta-aspartate':['L-beta-aspartate', 'beta-aspartate', 'L-beta-aspartyl', 'beta-aspartyl', 'L-beta-asp', 'beta-asp', '']}

def do_condensation(amino_acid_1, amino_acid_2, debug=0):
    '''Do a condensation reaction to form the peptide bond'''

    condensation_reaction = AllChem.ReactionFromSmarts("[#7:1]-[#6:2]-[#6:3](-[#8:4])=O.[#7:5]-[#6:6]-[#6:7](-[#8:8])=O >> "\
                                                       "[#7:1]-[#6:2]-[#6:3]([#7:5]-[#6:6]-[#6:7](-[#8:8])=O)=O.[H][#8:4][H]")

    if debug:
        print "Condensing %s and %s." % (amino_acid_1, amino_acid_2)

    aa1_mol = Chem.MolFromSmiles(amino_acid_1)
    aa2_mol = Chem.MolFromSmiles(amino_acid_2)

    products = condensation_reaction.RunReactants((aa1_mol, aa2_mol))

    if len(products) != 1:
        print "Possible products:", [Chem.MolToSmiles(element[0]) for element in products]
        raise Exception("Testing Error: Number of possible product sets not one. len(products):", len(products))

    if debug:
        for element in products:
            print Chem.MolToSmiles(element[0])
            print Chem.MolToSmiles(element[1])

    return Chem.MolToSmiles(products[0][0]) #products[0][0] is equivalent to element[0] in the prints above this line.

def do_condensation_alternative(amino_acid_1, amino_acid_2, debug=0):
    '''For removing ambiguity in a second condensation for a polypeptide with multiple carboxyl + amine groups that can be attached to.'''

    condensation_reaction = AllChem.ReactionFromSmarts("[#8:9]=[#6:10]-[#7:1]-[#6:2]-[#6:3](-[#8:4])=O.[#7:5]-[#6:6]-[#6:7](-[#8:8])=O >> "\
                                                       "[#8:9]=[#6:10]-[#7:1]-[#6:2]-[#6:3]([#7:5]-[#6:6]-[#6:7](-[#8:8])=O)=O.[H][#8:4][H]")

    if debug:
        print "Condensing (poly) %s and %s." % (amino_acid_1, amino_acid_2)

    aa1_mol = Chem.MolFromSmiles(amino_acid_1)
    aa2_mol = Chem.MolFromSmiles(amino_acid_2)

    products = condensation_reaction.RunReactants((aa1_mol, aa2_mol))

    if len(products) != 1:
        print "Possible products:", [Chem.MolToSmiles(element[0]) for element in products]
        raise Exception("Testing Error: Number of possible product sets not one. len(products):", len(products))

    if debug:
        for element in products:
            print Chem.MolToSmiles(element[0])
            print Chem.MolToSmiles(element[1])

    return Chem.MolToSmiles(products[0][0]) #products[0][0] is equivalent to element[0] in the prints above this line.    

def make_tripeptide(amino_acid_1, amino_acid_2, amino_acid_3, debug=0):
    '''Takes as input SMILES of 3 amino acids, order matters, returns tripeptides.

    Returns "Failed" on any error.'''

    try:
        dipeptide = do_condensation(amino_acid_1, amino_acid_2, debug)
        tripeptide = do_condensation(dipeptide, amino_acid_3, debug)
        return tripeptide
    except Exception as e:
        print e
        return "Failed."

def do_gamma_glutamate_condensation(amino_acid_1, amino_acid_2, debug=0):
    '''Condensate to gamma-glutamate, amino_acid_1 should be or contain glutamate.'''

    gamma_glutamate_condensation_reaction = AllChem.ReactionFromSmarts("[#8:1]-[#6:2](=[#8:15])-[#6:3]-[#6:4]-[#6:5](-[#7:9])-[#6:6](=[#8:7])-[#8:8]."\
                                                                       "[#8:10]-[#6:11](=[#8:12])-[#6:13]-[#7:14] >>"
                                                                       "[#8:10]-[#6:11](=[#8:12])-[#6:13]-[#7:14]-[#6:2](=[#8:15])-[#6:3]-[#6:4]-[#6:5](-[#7:9])-[#6:6](=[#8:7])-[#8:8]."
                                                                       "[H][#8:1][H]")

    if debug:   
        print "Gamma-Condensing %s and %s." % (amino_acid_1, amino_acid_2)    

    aa1_mol = Chem.MolFromSmiles(amino_acid_1)
    aa2_mol = Chem.MolFromSmiles(amino_acid_2)    

    products = gamma_glutamate_condensation_reaction.RunReactants((aa1_mol, aa2_mol))

    if len(products) != 1:
        print "Possible products:", [Chem.MolToSmiles(element[0]) for element in products]
        raise Exception("Testing Error: Number of possible product sets not one. len(products):", len(products))        

    main_product = Chem.MolToSmiles(products[0][0])
    if debug:
        print main_product
        print Chem.MolToSmiles(products[0][1]) #This should be water    

    return main_product        

def generate_tripeptides_containing_gamma_glutamate(debug=0):

    global amino_acid_dict

    gamma_glutamate_tripeptide_list = []
    gamma_glutamate_tripeptide_combination_list = []

    L_Glutamic_acid = amino_acid_dict["L-Glutamic acid"]
    if debug:
        print "Making gamma-Glutamylglutamic acid"
    gamma_glutamyl_glutamic_acid = do_gamma_glutamate_condensation(L_Glutamic_acid, L_Glutamic_acid, debug)

    combination_count = 0

    for amino_acid_1 in amino_acid_dict:
        for amino_acid_2 in amino_acid_dict:
            #Make tripeptide with gamma-glutamate on one end.
            if debug:
                print "Making tripeptide out of %s, %s and %s." % ("Gamma-glutamate", amino_acid_1, amino_acid_2)
            dipeptide = do_gamma_glutamate_condensation(L_Glutamic_acid, amino_acid_dict[amino_acid_1], debug)
            tripeptide = do_condensation_alternative(dipeptide, amino_acid_dict[amino_acid_2], debug)
            gamma_glutamate_tripeptide_list.append(tripeptide)
            gamma_glutamate_tripeptide_combination_list.append(" ".join(["Gamma-glutamate", amino_acid_1, amino_acid_2]))
            combination_count += 1

            #Make tripeptide with gamma-glutamate in the middle.
            if debug:
                print "Making tripeptide out of %s, %s and %s." % (amino_acid_1, "Gamma-glutamate", amino_acid_2)            
            dipeptide = do_condensation(amino_acid_dict[amino_acid_1], L_Glutamic_acid, debug)
            tripeptide = do_gamma_glutamate_condensation(dipeptide, amino_acid_dict[amino_acid_2], debug)
            gamma_glutamate_tripeptide_list.append(tripeptide)
            gamma_glutamate_tripeptide_combination_list.append(" ".join([amino_acid_1, "Gamma-glutamate", amino_acid_2])) 
            combination_count += 1

        #Make tripeptide with two gamma-glutamates.
        if debug:
            print "Making tripeptide out of %s, %s and %s." % ("Gamma-glutamate", "Gamma-glutamate", amino_acid_1) 
        tripeptide = do_gamma_glutamate_condensation(gamma_glutamyl_glutamic_acid, amino_acid_dict[amino_acid_1], debug)
        gamma_glutamate_tripeptide_list.append(tripeptide)
        gamma_glutamate_tripeptide_combination_list.append(" ".join(["Gamma-glutamate", "Gamma-glutamate", amino_acid_1])) 
        combination_count += 1

    return gamma_glutamate_tripeptide_list, gamma_glutamate_tripeptide_combination_list, combination_count

def do_beta_aspartate_condensation(amino_acid_1, amino_acid_2, debug=0):
    '''Condensate to beta-aspartate, amino_acid_1 should be or contain L-aspartate.'''

    beta_aspartate_condensation_reaction = AllChem.ReactionFromSmarts("[#8:1]-[#6:2](=[#8:15])-[#6:3]-[#6:4](-[#7:9])-[#6:6](=[#8:7])-[#8:8]."\
                                                                      "[#8:10]-[#6:11](=[#8:12])-[#6:13]-[#7:14] >>"
                                                                      "[#8:10]-[#6:11](=[#8:12])-[#6:13]-[#7:14]-[#6:2](=[#8:15])-[#6:3]-[#6:4](-[#7:9])-[#6:6](=[#8:7])-[#8:8]."
                                                                      "[H][#8:1][H]")

    if debug:   
        print "Beta-Condensing %s and %s." % (amino_acid_1, amino_acid_2)    

    aa1_mol = Chem.MolFromSmiles(amino_acid_1)
    aa2_mol = Chem.MolFromSmiles(amino_acid_2)    

    products = beta_aspartate_condensation_reaction.RunReactants((aa1_mol, aa2_mol))

    if len(products) != 1:
        print "Possible products:", [Chem.MolToSmiles(element[0]) for element in products]
        raise Exception("Testing Error: Number of possible product sets not one. len(products):", len(products))        

    main_product = Chem.MolToSmiles(products[0][0])
    if debug:
        print main_product
        print Chem.MolToSmiles(products[0][1]) #This should be water    

    return main_product    
def generate_tripeptides_containing_beta_aspartate(debug=0):

    global amino_acid_dict

    beta_aspartate_tripeptide_list = []
    beta_aspartate_tripeptide_combination_list = []

    L_aspartic_acid = amino_acid_dict["L-Aspartic acid"]

    if debug:
        print "Making beta-aspartate dipeptide"   
    beta_aspartate_dipeptide = do_beta_aspartate_condensation(L_aspartic_acid, L_aspartic_acid, debug)

    combination_count = 0

    for amino_acid_1 in amino_acid_dict:
        for amino_acid_2 in amino_acid_dict:
            #Make tripeptide with beta-aspartate on one end.
            if debug:
                print "Making tripeptide out of %s, %s and %s." % ("Beta-aspartate", amino_acid_1, amino_acid_2)
            dipeptide = do_beta_aspartate_condensation(L_aspartic_acid, amino_acid_dict[amino_acid_1], debug)
            tripeptide = do_condensation_alternative(dipeptide, amino_acid_dict[amino_acid_2], debug)
            beta_aspartate_tripeptide_list.append(tripeptide)
            beta_aspartate_tripeptide_combination_list.append(" ".join(["Beta-aspartate", amino_acid_1, amino_acid_2]))
            combination_count += 1

            #Make tripeptide with beta-aspartate in the middle.
            if debug:
                print "Making tripeptide out of %s, %s and %s." % (amino_acid_1, "Beta-aspartate", amino_acid_2)            
            dipeptide = do_condensation(amino_acid_dict[amino_acid_1], L_aspartic_acid, debug)
            tripeptide = do_beta_aspartate_condensation(dipeptide, amino_acid_dict[amino_acid_2], debug)
            beta_aspartate_tripeptide_list.append(tripeptide)
            beta_aspartate_tripeptide_combination_list.append(" ".join([amino_acid_1, "Beta-aspartate", amino_acid_2])) 
            combination_count += 1

        #Make tripeptide with two beta-aspartates.
        if debug:
            print "Making tripeptide out of %s, %s and %s." % ("Beta-aspartate", "Beta-aspartate", amino_acid_1) 
        tripeptide = do_beta_aspartate_condensation(beta_aspartate_dipeptide, amino_acid_dict[amino_acid_1], debug)
        beta_aspartate_tripeptide_list.append(tripeptide)
        beta_aspartate_tripeptide_combination_list.append(" ".join(["Beta-aspartate", "Beta-aspartate", amino_acid_1])) 
        combination_count += 1

    return beta_aspartate_tripeptide_list, beta_aspartate_tripeptide_combination_list, combination_count    

def generate_tripeptide_synonyms(tripeptide_combination_string):

    global amino_acid_synonym_dict

    aa_list = re.split(" (?!acid)", tripeptide_combination_string) #Split by space except where followeed by acid.
    
    if len(aa_list) != 3:
        raise ValueError("Tripeptide with more or less than 3 amino acids:", aa_list)
    
    name_list = []
    
    name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][3], amino_acid_synonym_dict[aa_list[1]][3], 
                               amino_acid_synonym_dict[aa_list[2]][1]]).title())
    
    name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][2], amino_acid_synonym_dict[aa_list[1]][2], 
                               amino_acid_synonym_dict[aa_list[2]][0]]).title())
    
    name_list.append("".join([amino_acid_synonym_dict[aa_list[0]][3], amino_acid_synonym_dict[aa_list[1]][3], 
                               amino_acid_synonym_dict[aa_list[2]][1]]))  
    
    name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][4], amino_acid_synonym_dict[aa_list[1]][4], 
                               amino_acid_synonym_dict[aa_list[2]][4]]).title())    
    
    name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][5], amino_acid_synonym_dict[aa_list[1]][5], 
                               amino_acid_synonym_dict[aa_list[2]][5]]).title())     
    
    name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][1], amino_acid_synonym_dict[aa_list[1]][1], 
                               amino_acid_synonym_dict[aa_list[2]][1]]).title() + " tripeptide") 
    
    name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][0], amino_acid_synonym_dict[aa_list[1]][0], 
                               amino_acid_synonym_dict[aa_list[2]][0]]).title() + " tripeptide")         
    
    if min(len(amino_acid_synonym_dict[aa_list[0]][6]), len(amino_acid_synonym_dict[aa_list[1]][6]), 
           len(amino_acid_synonym_dict[aa_list[1]][6])) > 0:
        name_list.append("".join([amino_acid_synonym_dict[aa_list[0]][6], amino_acid_synonym_dict[aa_list[1]][6], 
                         amino_acid_synonym_dict[aa_list[2]][6]])+" tripeptide")
        
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][6], amino_acid_synonym_dict[aa_list[1]][6], 
                                      amino_acid_synonym_dict[aa_list[2]][6]])+" tripeptide")   
    
    if "ic acid" in tripeptide_combination_string:
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][3], amino_acid_synonym_dict[aa_list[1]][3], 
                                   amino_acid_synonym_dict[aa_list[2]][1]]).replace("ic acid", "ate").title())   
        
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][2], amino_acid_synonym_dict[aa_list[1]][2], 
                                   amino_acid_synonym_dict[aa_list[2]][0]]).replace("ic acid", "ate").title())   
        
        name_list.append("".join([amino_acid_synonym_dict[aa_list[0]][3], amino_acid_synonym_dict[aa_list[1]][3], 
                                  amino_acid_synonym_dict[aa_list[2]][1]]).replace("ic acid", "ate"))   
        
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][1], amino_acid_synonym_dict[aa_list[1]][1], 
                                   amino_acid_synonym_dict[aa_list[2]][1]]).replace("ic acid", "ate").title() + " tripeptide") 
    
        name_list.append("-".join([amino_acid_synonym_dict[aa_list[0]][0], amino_acid_synonym_dict[aa_list[1]][0], 
                                   amino_acid_synonym_dict[aa_list[2]][0]]).replace("ic acid", "ate").title() + " tripeptide")         
    
    #Remove any duplicates. eg. for "Glycine Glycine Glycine"
    new_name_list = []
    for element in name_list:
        if element not in new_name_list:
            new_name_list.append(element.strip("-").replace("--", "-")) #Remove leading and trailing "-" and double "-", for L-3-hydroxyproline.
    name_list = new_name_list
    
    return name_list

def generate_tripeptides(debug=0):

    global amino_acid_dict

    #List of generated tripeptides
    tripeptide_smiles_list = []

    #For debugging: List of amino acid combinations combinations in same order as tripeptide_smiles_list.
    tripeptide_combination_list = []

    #List of any combinations of amino acids for which tripeptide generation failed.
    failed_combination_list = []

    combination_count = 0

    for amino_acid_1 in amino_acid_dict:
        for amino_acid_2 in amino_acid_dict:
            for amino_acid_3 in amino_acid_dict:
                if debug:
                    print "Making tripeptide out of %s, %s and %s." % (amino_acid_1, amino_acid_2, amino_acid_3)
                result = make_tripeptide(amino_acid_dict[amino_acid_1], amino_acid_dict[amino_acid_2], 
                                         amino_acid_dict[amino_acid_3], debug)
                if result == "Failed.":
                    failed_combination_list.append([amino_acid_1, amino_acid_2, amino_acid_3])
                else:
                    tripeptide_smiles_list.append(result)
                    tripeptide_combination_list.append(" ".join([amino_acid_1, amino_acid_2, amino_acid_3]))

                combination_count += 1
                if combination_count % 100 == 0:
                    print "Processed", combination_count, "combinations"

    #Handle gamma-glutamate as a special case
    print "Processing gamma-glutamate combinations"
    gamma_glutamate_tripeptide_list, gamma_glutamate_tripeptide_combination_list, gamma_glutamate_combination_count = \
        generate_tripeptides_containing_gamma_glutamate(debug)

    tripeptide_smiles_list += gamma_glutamate_tripeptide_list
    tripeptide_combination_list += gamma_glutamate_tripeptide_combination_list
    combination_count += gamma_glutamate_combination_count

    #Handle beta-aspartate as a special case
    print "Processing beta-aspartate combinations"
    beta_aspartate_tripeptide_list, beta_aspartate_tripeptide_combination_list, beta_aspartate_combination_count = \
        generate_tripeptides_containing_beta_aspartate(debug)

    tripeptide_smiles_list += beta_aspartate_tripeptide_list
    tripeptide_combination_list += beta_aspartate_tripeptide_combination_list
    combination_count += beta_aspartate_combination_count

    #Write output
    if not os.path.exists("outputs"):
        os.mkdir("outputs")

    with open("outputs/generated_tripeptides.txt", "wb") as output_f, \
         open("outputs/generated_tripeptides_detailed.txt", "wb") as output_f2, \
         open("outputs/generated_tripeptides_more_detailed.csv", "wb") as output_f3: #For more detailed output, include name and synonyms.

        csv_w = csv.DictWriter(output_f3, fieldnames=["SMILES", "Amino Acids used", "Names"])
        csv_w.writeheader()

        for smiles, combination in zip(tripeptide_smiles_list, tripeptide_combination_list):
            output_f.write(smiles + "\r\n")
            output_f2.write(combination + "\t" + smiles + "\r\n")            
            csv_w.writerow({"SMILES":smiles, "Amino Acids used":combination, "Names":"; ".join(generate_tripeptide_synonyms(combination))})

    with open("outputs/failed_combinations.txt", "wb") as output_f:
        for element in failed_combination_list:
            output_f.write(str(element) + "\r\n")

    print "Succeeded:", len(tripeptide_smiles_list)
    print "Failed:", len(failed_combination_list)

if __name__ == "__main__":
    generate_tripeptides(debug=0)
    print "Finished in", datetime.now() - start_time

