from generate_cyclic_dipeptides import amino_acid_synonym_dict, generate_cyclic_dipeptide_synonyms

def test_generate_cyclic_dipeptide_synonyms():
    test_strings = ["L-Leucine L-4-Hydroxyproline", "L-Isoleucine L-Asparagine", "L-Valine L-Glutamic acid", "Glycine Glycine", 
                    "Gamma-glutamate L-Aspartic acid", "L-Serine L-3-Hydroxyproline"]
    
    for test_string in test_strings:
    
        for element in generate_cyclic_dipeptide_synonyms(test_string):
            print element    
    
        print "----------------------------------------------------------------------------------------------------------"
    
if __name__ == "__main__":
    test_generate_cyclic_dipeptide_synonyms()


