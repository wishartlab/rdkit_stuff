from generate_tripeptides import amino_acid_synonym_dict, generate_tripeptide_synonyms

def test_generate_tripeptide_synonyms():
    test_strings = ["L-4-Hydroxyproline Beta-aspartate L-Lysine", "L-Isoleucine L-Asparagine L-Valine", "L-Leucine L-Valine L-Glutamic acid",
                    "Glycine Glycine Glycine", "Glycine Glycine L-Glutamic acid", "L-Threonine L-Serine L-3-Hydroxyproline"]
    
    for test_string in test_strings:
    
        for element in generate_tripeptide_synonyms(test_string):
            print element    
    
        print "----------------------------------------------------------------------------------------------------------"
    
if __name__ == "__main__":
    test_generate_tripeptide_synonyms()


